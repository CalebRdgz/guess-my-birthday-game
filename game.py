# from random import randint
# from tkinter.messagebox import YES

# name = input("Hi! What is your name?")
# print("Hi,",name, "!")

# month = randint(1,12)
# year = randint(1990,2004)
# i = 0



# while i < 5:
#     print("Were you born in",month,"/",year,"?")
#     response = input("yes or no?")
#     if response == "no":
#         print("Drat! Lemme try again!")
#         i = i+1
#     elif response == "yes":
#         print("I knew it!")
# print("I have other things to do. Good bye.")

# if response == "no":
#     print("Drat! Lemme try again!")
#     i = i+1

# if i >= 4:
#     print("I have other things to do. Good bye.")

# if response == "yes":
#     print("I knew it!")

#----------------------------------------------------------

# from random import randint

# name = input("Hi! What is your name? ")

# # Guess 1

# month_number = randint(1, 12)
# year_number = randint(1924, 2004)

# print("Guess 1 :", name, "were you born in",
#       month_number, "/", year_number, "?")

# response = input("yes or no? ")

# if response == "yes":
#     print("I knew it!")
#     exit()
# else:
#     print("Drat! Lemme try again!")

# # Guess 2

# month_number = randint(1, 12)
# year_number = randint(1924, 2004)

# print("Guess 2 :", name, "were you born in",
#       month_number, "/", year_number, "?")

# response = input("yes or no? ")

# if response == "yes":
#     print("I knew it!")
#     exit()
# else:
#     print("Drat! Lemme try again!")

# # Guess 3

# month_number = randint(1, 12)
# year_number = randint(1924, 2004)

# print("Guess 3 :", name, "were you born in",
#       month_number, "/", year_number, "?")

# response = input("yes or no? ")

# if response == "yes":
#     print("I knew it!")
#     exit()
# else:
#     print("Drat! Lemme try again!")

# # Guess 4

# month_number = randint(1, 12)
# year_number = randint(1924, 2004)

# print("Guess 4 :", name, "were you born in",
#       month_number, "/", year_number, "?")

# response = input("yes or no? ")

# if response == "yes":
#     print("I knew it!")
#     exit()
# else:
#     print("Drat! Lemme try again!")

# # Guess 5

# month_number = randint(1, 12)
# year_number = randint(1924, 2004)

# print("Guess 5 :", name, "were you born in",
#       month_number, "/", year_number, "?")

# response = input("yes or no? ")

# if response == "yes":
#     print("I knew it!")
#     exit()
# else:
#     print("I have other things to do. Good bye.")

#---------------------------------------------------------------

from random import randint

num_guesses = input("How many guesses should the computer get?")
num_guesses = int(num_guesses)

# Get the game started: print out the game introduction
input("choose a number between 1 and 50 (press enter when ready)\n")

# variables for game state
low = 1  # low limit for possible guesses
high = 50  # high limit for possible guesses

for guess_num in range(num_guesses):
    print(guess_num)
# calculate the next guess
guess = int((low + high) / 2)
guesses = []

# print out the guess and get player's response
print("\nIs", {guess}, "your number? (correct, higher, lower)".format(guess=guess))
answer = input("Answer: ")

if answer == "correct":
    print("\nThe computer guessed your number: ", guess)
    print(guesses)
    exit()
elif answer == "lower":
    # if the answer is too high
    guesses.append([answer, guess])
    high = guess - 1
elif answer == "higher":
    # if the answer is too low
    guesses.append([answer, guess])
    low = guess + 1
else:
    # unknown input
    print("\nbad input try again\n")

print("\nThe computer didn't guess your number, you win!")